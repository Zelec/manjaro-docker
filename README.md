# [Docker Base Image for Manjaro Linux](https://gitlab.com/Zelec/manjaro-docker)
#### Forked from [Manjaro's Docker Images](https://gitlab.manjaro.org/tools/development-tools/manjaro-docker)
This repository contains all scripts and files needed to create a Docker base image for the Manjaro Linux distribution.

## Usage
You can pull from dockerhub at [zelec/manjarolinux](https://hub.docker.com/r/zelec/manjarolinux) Or follow the instructions below to build it locally

## Dependencies
Install the following Manjaro Linux packages:
* make
* manjaro-tools-base
* docker
## Building
Run `make ci` to build the base image using docker  
Or run `make docker-image` as root to build the base image directly on your machine.
## Purpose
* Provide the Manjaro experience in a Docker Image
* Provide the most simple but complete image to base every other upon
* `pacman` needs to work out of the box
* All installed packages have to be kept unmodified
